"use strict"

import * as usefulFunctions from "./components/functions.js"; // Полезные функции
import maskInput from './forms/input-mask.js'; // Маска ввода для форм
import mobileNav from './components/mobile-nav.js';  // Мобильное меню
import { Fancybox } from "@fancyapps/ui"; // Fancybox modal gallery
import intlTelInput from 'intl-tel-input';
import Spoilers from "./components/spoilers.js";
import {WebpMachine} from "webp-hero"

// Проверка поддержки webp
usefulFunctions.isWebp();

// Добавление класса после загрузки страницы
usefulFunctions.addLoadedClass();

// Добавление класса touch для для мобильных
usefulFunctions.addTouchClass()

// Mobile 100vh
usefulFunctions.fullVHfix();

// IE Webp Support
const webpMachine = new WebpMachine();
webpMachine.polyfillDocument();

// Phone field
const input = document.querySelector('#phone');
if (input) {
    intlTelInput(input, {
        initialCountry: "ru",
        nationalMode: true,
        formatOnDisplay: true,
        separateDialCode: true,
        hiddenInput: "full_number",
        localizedCountries: { 'ru': 'Russian' },
        utilsScript: "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.13/js/utils.min.js",
    });
}

// Маска для ввода номера телефона
maskInput('input[name="phone"]');

// Меню для мобильной версии
mobileNav();

// Spoilers
Spoilers();

// Scroll
const anchors = document.querySelectorAll('a[href*="#"]')
for (let anchor of anchors) {
    anchor.addEventListener('click', function (e) {
        e.preventDefault()

        const blockID = anchor.getAttribute('href').substr(1)

        document.getElementById(blockID).scrollIntoView({
            behavior: 'smooth',
            block: 'start'
        })
    })
}


// Modal Fancybox
Fancybox.bind('[data-fancybox]', {
    autoFocus: false
});
